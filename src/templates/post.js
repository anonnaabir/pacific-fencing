import React, { Component } from "react"
import { graphql } from "gatsby"
import PropTypes from "prop-types"
import Layout from "../components/layout"
import Header from "../components/header"
import Footer from "../components/footer"
import Testimonial from "../components/testimonial";
import StickyButton from "../components/sticky-button";
import SEO from "../components/seo";
import "../style/bootstrap.min.css";
import "../style/style.css";
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';

class Post extends Component {
  render() {
    const post = this.props.data.wordpressPost

    return (
      <>
        <Header />
        <Jumbotron>
          <div className="page-header">
          <h1 className="ContentTitle" >{post.title}</h1>
          </div>
  
</Jumbotron>
      <div class="container PageContent">
        <div className="ContentDescription" dangerouslySetInnerHTML={{ __html: post.content }} />
        </div>
        <Testimonial />
        <Footer />
        <StickyButton />
      </>
    )
  }
}

Post.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default Post

export const postQuery = graphql`
  query($id: String!) {
    wordpressPost(id: { eq: $id }) {
      title
      content
    }
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`