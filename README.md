<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="http://pacific-fencing.com.au/">
    <img alt="Gatsby" src="http://pacific-fencing.com.au/wp-content/uploads/2018/03/PacificFencing1.png" width="200" />
  </a>
</p>
<h1 align="center">
  Pacific Fencing-Headless Version
</h1>

Kick off your project with this default boilerplate. This starter ships with the main Gatsby configuration files you might need to get up and running blazing fast with the blazing fast app generator for React.

_Have another more specific idea? You may want to check out our vibrant collection of [official and community-created starters](https://www.gatsbyjs.org/docs/gatsby-starters/)._

## 🚀 Start Developing

To develop the site in local, we need to follow these steps:
- Clone it through Github Desktop.
- Enter the command in Terminal
    
    ```shell
    cd pacific-fencing
    sudo npm install
    gatsby develop
    ```

## 💫 Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)

[![Deploy with ZEIT Now](https://zeit.co/button)](https://zeit.co/import/project?template=https://github.com/gatsbyjs/gatsby-starter-default)

<!-- AUTO-GENERATED-CONTENT:END -->
